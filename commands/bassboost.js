const Command = require('../lib/Command')
const Discord = require('discord.js')
const Song = require('../music/Song')

module.exports = class extends Command {

    constructor(...args) {
        super(...args)
        this.name = "bassboost"
        this.description = "Loud = funni"
        this.guilds = []
        this.options = [

        ]
    }

    async execute(msg) {
        msg.ack()
        var editmessage = await msg.channel.send(`<a:loading2:579334970741620746> working`)
        var player = this.client.playerManager.players.get(msg.guild.id)
        if (!player || !player.playing) {
            return editmessage.edit('There is no queue playing in this server!')
        }
        if (player._bassboost) {
            player.bassboost(false)
            return editmessage.edit(`Turned **OFF** bassboost for this server.`).then(delete5)
        } else {
            player.bassboost(true)
            return editmessage.edit(`Turned **ON** bassboost for this server.`).then(delete5)
        }

    }

    async init() {
        // You can optionally define this method which will be run when the bot starts (after login, so discord data is available via this.client)
    }

};

function delete5(m) {
    setTimeout(() => m.delete(), 5000)
}