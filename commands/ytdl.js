const Command = require('../lib/Command')
const Discord = require('discord.js')
const Song = require('../music/Song')
const fetch = require('node-fetch')
module.exports = class extends Command {

    constructor(...args) {
        super(...args)
        this.name = "ytdl"
        this.description = "Skips to the next song in the queue (If one is available)"
        this.guilds = ["663818475973902360", "771143772880502814"]
        this.options = [
            {
                name: "action",
                description: "What to do?",
                required: true,
                type: 3,
                choices: [
                    {
                        name: "search",
                        value: "search"
                    },
                    {
                        name: "download",
                        value: "download"
                    }
                ]
            },
            {
                name: "arg",
                description: "Name of Song to Search | ID to convert",
                type: 3,
                required: true
            }
        ]
    }

    async execute(msg) {
        msg.ack()
        var editmessage = await msg.channel.send(`<a:loading2:579334970741620746> working`)
        if (msg.args.get("action") == "search") {
            // get song info and store / present
            var options = await fetch("http://localhost:6669/search", {
                method: "post",
                body: JSON.stringify({
                    search: msg.args.get("arg")
                }),
                headers: { 'Content-Type': 'application/json' }
            }).then(r => r.json())
            var e = new Discord.MessageEmbed()
            e.setTimestamp()
                .setTitle(`Songs for ${msg.args.get("arg")}`)
                .setFooter(options.nonce)
                .setAuthor(msg.author.tag, msg.author.avatarURL())
                .setColor(0xff0f0f)
                .setDescription("Use the ID provided underneath a result with `/ytdl download`")
            e.addField(options.one.title, options.one.id)
            e.addField(options.two.title, options.two.id)
            e.addField(options.three.title, options.three.id)
            e.addField(options.four.title, options.four.id)
            e.addField(options.five.title, options.five.id)
            editmessage.delete()
            msg.channel.send(e)
            this.client.inSorting.set(msg.author.id, {nonce: options.nonce, search: msg.args.get("arg")})
        } else {
            var id = this.client.inSorting.get(msg.author.id)
            if (!id) {
                editmessage.edit(":x: You have not searched for a song recently!")
                return;
            }
            // download song using id
            var error = false;
            var response = await fetch("http://localhost:6669/download", {
                method: "post",
                body: JSON.stringify({
                    nonce: id.nonce,
                    option: msg.args.get("arg")
                }),
                headers: { 'Content-Type': 'application/json' }
            }).then(r => r.buffer())
            .catch(() => {
                editmessage.edit(":x: Invalid ID! Perhaps you copied it wrong?")
                error = true;
            })
            if (error) {
                return;
            }
            await editmessage.edit("<a:loading2:579334970741620746> uploading")
            await msg.channel.send(new Discord.MessageAttachment(response, `${id.search}.mp3`))
            editmessage.delete()
        }
    }

    async init() {
        // You can optionally define this method which will be run when the bot starts (after login, so discord data is available via this.client)
    }

};

function delete5(m) {
    setTimeout(() => m.delete(), 5000)
}