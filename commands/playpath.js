const Command = require('../lib/Command')
const Discord = require('discord.js')
const Song = require('../music/Song')

module.exports = class extends Command {

    constructor(...args) {
       /* super(...args, {
            name: 'play',
            enabled: true,
            runIn: ['text'],
            cooldown: 0,
            aliases: ["p"],
            permLevel: 0,
            botPerms: ['MANAGE_MESSAGES'],
            requiredSettings: [],
            description: 'Add a song to the queue',
            quotedStringSupport: false,
            usage: '<Song:string>',
            usageDelim: undefined,
            extendedHelp: 'No extended help available.'
        });*/
        super(...args)
        this.name = "playpath"
        this.description = "Play a mp3 file on the bot [Owner Only]"
        this.guilds = ["663818475973902360", "776133474129543209", "517833347754360832"]
        this.options = [
            {
                name: "path",
                description: "Path to the mp3 file",
                type: 3,
                required: true
            },
            {
                name: "next",
                description: "Make this song the next played one?",
                type: 5
            }
        ]
    }


    async execute(msg, search) {
        //console.log(search)
        msg.ack()
        var search = msg.args.get("path")
       /* if (this.client.lockState.has(msg.guild.id)) {
            var b = await this.client.permissionLevels.run(msg, parseInt(this.client.lockState.get(msg.guild.id)))
            console.log(b)
            if (!b.permission) {
                return msg.channel.send(`:x: This server requires you to have a permission level of at least **${this.client.lockState.get(msg.guild.id)}**.`).then(c => c.delete(5000))
            }
        }*/

        var editmessage = await msg.channel.send(`<a:loading2:579334970741620746> searching for **${search}** in your homework folder`)
        var song;
        var trycounter = 1;
        while (!song) {
            if (trycounter > 10) {
                await editmessage.edit(":x: Failed to fetch song info after 10 tries, is Youtube down?");
                return;
            }
            if (search.includes('youtube.com')) {
                var songs = await getSongs(search)
            } else {
                var songs = await getSongs(`${search}`)
            }
            song = songs[0]

            if (!song) {
                //return editmessage.edit(":x: Error searching for song")
                await editmessage.edit(`<a:loading2:579334970741620746> Youtube returned an error, please wait... [${trycounter}]`)
            }
            trycounter++;
        }
        //this.client.console.log(JSON.stringify(song))
        editmessage.edit(`<a:checkmark:615636862249336967> Adding **${song.info.title}** by **${song.info.author}** to the playing queue [${msg.author.tag}] ${song.info.title.toLowerCase().includes('juice wrld') ? "(RIP JUICE)" : ""}`).then(delete5)
        /*if (song.info.title.toLowerCase().includes('juice wrld')) {
            msg.channel.send('RIP juice').then(c => c.delete(4000))
        }*/
        var x = this.client.playerManager.join({
            guild: msg.guild.id,
            channel: msg.member.voice.channel.id,
            host: "localhost"
        })
        x.registerTextChannel(msg.channel)
        await x.init()
        await x.queueSongs(new Song(song, msg.author.tag, search, null),  msg.args.get("next"))
        if (x.queue.length == 1) {
            x.current = x.queue[0]
            //x.oldqueue = [new Song(song, msg.author.tag)]
            if (msg.channel.flags && msg.channel.flags.get('default_volume') != undefined) {
                x.volume(parseInt(msg.channel.flags.get('default_volume')))
                x._channel.send(`Respecting \`default_volume\` flag, setting volume to **${msg.channel.flags.get('default_volume')}**`).then(c => c.delete({timeout: 6000}))
            }
            x.play(x.current.track)
            await x._channel.send(new Discord.MessageAttachment(x.current.img, 'render.png')).then(delete5)
            //x._channel.send( `Now playing **${x.current.title}** [${x.current.tag}]`).then(m => m.delete({timeout: 3000}))

        }

    }

    async init() {
        // You can optionally define this method which will be run when the bot starts (after login, so discord data is available via this.client)
    }

};

const fetch = require("node-fetch");
const { URLSearchParams } = require("url");

async function getSongs(search) {

    const params = new URLSearchParams();
    params.append("identifier", search);

    return fetch(`http://localhost:2333/loadtracks?${params.toString()}`, { headers: { Authorization: 'i' } })
        .then(res => res.json())
        .then(data => data.tracks)
        .catch(err => {
            console.error(err);
            return null;
        });
}

function delete5(m) {
    setTimeout(() => m.delete(), 5000)
}