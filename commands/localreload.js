const Command = require('../lib/Command')
const Discord = require('discord.js')


module.exports = class extends Command {

    constructor(...args) {
        super(...args)
        this.name = "localreload"
        this.description = "Reloads the local code, does not refresh commands on discord's side"
        this.guilds = ["517833347754360832"]
        this.options = [

        ]
    }

    async execute(msg) {
       
        if (msg.author.id != "289498936773967872") {
            msg.ack()
            return;
        }
        var e = await this.client.commandManager.reload_local()
        msg.respond(4, `Loaded **${e}** commands`)
    }

    async init() {
        // You can optionally define this method which will be run when the bot starts (after login, so discord data is available via this.client)
    }

};

function delete5(m) {
    setTimeout(() => m.delete(), 5000)
}