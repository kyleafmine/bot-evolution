const Command = require('../lib/Command')
const Discord = require('discord.js')
const Song = require('../music/Song')

module.exports = class extends Command {

    constructor(...args) {
        super(...args)
        this.name = "skip"
        this.description = "Skips to the next song in the queue (If one is available)"
        this.guilds = []
        this.options = [
            {
                name: "count",
                description: "Number of songs to skip",
                type: 4
            }
        ]
    }

    async execute(msg) {
        msg.ack()
        var editmessage = await msg.channel.send(`<a:loading2:579334970741620746> working`)
        var player = this.client.playerManager.players.get(msg.guild.id)
        if (!player || !player.playing) {
            return editmessage.edit('There is no queue playing in this server!')
        }
        if (msg.args.get("count") && (player.queue.length - msg.args.get("count")) < 1) {
            editmessage.edit(':x: Cannot skip that many songs!')
            return;
        }
        if (player.queue.length > 1) {
            if (msg.args.get("count")) {
                player.next(false, msg.args.get("count"))
                return editmessage.edit(`<a:checkmark:615636862249336967> Skipped ${msg.args.get("count")} songs!`).then(delete5)
            } else {
                player.next()
                return editmessage.edit('<a:checkmark:615636862249336967> Skipped song!').then(delete5)
            }
        } else {
            editmessage.edit(':x: No songs left to skip!')
        }
    }

    async init() {
        // You can optionally define this method which will be run when the bot starts (after login, so discord data is available via this.client)
    }

};

function delete5(m) {
    setTimeout(() => m.delete(), 5000)
}