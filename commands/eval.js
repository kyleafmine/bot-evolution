
const Command = require('../lib/Command')
const Discord = require('discord.js')
const inspect = require("util").inspect

const fetch = require("node-fetch");
module.exports = class extends Command {

	constructor(...args) {
        super(...args)
        this.name = "eval"
        this.description = "[Reserved for Owner]"
        this.guilds = ["663818475973902360", "517833347754360832"]
        this.options = [
            {
                name: "expression",
                description: "JavaScript",
                type: 3,
                required: true
            }
        ]
	}

	async execute(msg) {
        if (msg.author.id != "289498936773967872") {
            msg.ack()
            return;
        }
		try {
			let evaled = eval(msg.args.get("expression"));
			if (evaled instanceof Promise) evaled = await evaled;
			if (typeof evaled !== 'string') evaled = inspect(evaled, { depth: 0 });
			msg.respond(4, "```js\n" + evaled + "\n```")
		} catch (err) {
			msg.respond(4, ` \`ERROR\`\n\`\`\` ${require('util').inspect(err)}\`\`\``);
			if (err.stack) this.client.emit('error', err.stack);
		}
	}

};

async function getSongs(search) {
 
    const params = new URLSearchParams();
    params.append("identifier", search);
 
    return fetch(`http://localhost:2333/loadtracks?${params.toString()}`, { headers: { Authorization: 'i' } })
        .then(res => res.json())
        .then(data => data.tracks)
        .catch(err => {
            console.error(err);
            return null;
        });
}
async function getPlaylistData(id) {
    const params = new URLSearchParams();
    params.append("id", id)
    params.append("part", "snippet")
    params.append("key", "AIzaSyCmp7DXn9w2wqexHKFoyXCYdT-lLgkWxgM")
    var response = await fetch(`https://www.googleapis.com/youtube/v3/playlists?${params.toString()}`, { headers: { Accept: 'application/json'}}).then(r => r.json())
    return response.items[0].snippet.title
}
function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}