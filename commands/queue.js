const Command = require('../lib/Command')
const Discord = require('discord.js')
const Song = require('../music/Song')
module.exports = class extends Command {

    constructor(...args) {
        super(...args)
        this.name = "queue"
        this.description = "Displays all queued tracks in this server"
        this.guilds = []
        this.options = [

        ]
    }

    async execute(msg) {
        msg.ack()
        var editmessage = await msg.channel.send(`<a:loading2:579334970741620746> working`)
        var player = this.client.playerManager.players.get(msg.guild.id)
        if (!player || !player.playing) {
            return editmessage.edit('There is no queue playing in this server!')
        }
        var queue = []
        player.oldqueue.forEach(i => queue.push(i))
        player.queue.forEach(i => queue.push(i))
        var ee = ""
        for (var i of msg.guild.name.split('')) {
            ee += "="
        }
        var msg1 = `\`\`\`asciidoc\n${msg.guild.name}\n${ee}\n`
        msg1 += `\nvolume :: ${player._volume}`
        msg1 += `\nchannel :: 🔊 ${msg.guild.channels.cache.get(player.channel).name}`
        msg1 += `\nlooping :: ${player.looping ? "yes" : "no"}`
        msg1 += "\n```\n"
        var msgg = "```python\n"
        if (queue.length > 10) {
            // total songs = 4 before, 4 after for total of 10
            var startpos = player.oldqueue.length - 4
            var endpos = player.oldqueue.length + 4
            console.log(startpos, endpos)
        } else {
            
        }
        for (var i in queue) {
            if (startpos !=null && endpos) {
                if (i > endpos || i < startpos) {
                    continue;
                }
            }
            msgg += `\n${parseInt(i) + 1} > "${queue[i].title.split('').filter(c => c != "\"").join('')}" [${queue[i].tag.replace("#", "ⵌ")}]`
            if (player.oldqueue.length == i) {
                msgg += '\n#   you are here ⬏'
            }
        }
        
        msgg +="\n```"
        editmessage.edit(msg1 + msgg).then(delete15)
    }
};

function delete15(m) {
    setTimeout(() => m.delete(), 15000)
}