
const Command = require('../lib/Command')
const Discord = require('discord.js')
module.exports = class extends Command {

    constructor(...args) {
        super(...args)
        this.name = "playing"
        this.description = "Fetches the currently playing song's card"
        this.guilds = ["517833347754360832"]
        this.options = [

        ]
    }

    async execute(msg) {
        msg.ack()
        var editmessage = await msg.channel.send(`<a:loading2:579334970741620746> working`)
        var player = this.client.playerManager.players.get(msg.guild.id)
        if (!player || !player.playing) {
            return editmessage.edit('There is no queue playing in this server!')
        }
        await msg.channel.send(new Discord.MessageAttachment(player.current.img, 'PlayerInternals.RenderedImage.fullRender.png'))
        editmessage.delete()
    }

    async init() {
        // You can optionally define this method which will be run when the bot starts (after login, so discord data is available via this.client)
    }

};

