
const { PlayerManager } = require('discord.js-lavalink')
const { Collection } = require('discord.js')
const Discord = require('discord.js')
const fs = require('fs')
const  CommandManager = require('./CommandManager')
const  TestCommand  = require('./TestCommand')
const  Interaction  = require('./Interaction')
module.exports = class NamelessClient extends Discord.Client {
    constructor(...args) {
        super(...args)

        this.r = require('rethinkdbdash')({
            db: 'new_discord'
        })
        this.playerManager = null
        this.playlists = new Map()
        this.blending = false
        this.inSorting = new Map()
        this.invites = {}
        this.lockState = new Map()
        this.scheduler = require('node-schedule')
        this.commandManager = new CommandManager(this);
        this.on('ready', async () => {
            await this.commandManager.checkCommands()
            this.playerManager =  new PlayerManager(this, [{'host': 'localhost', 'port': 2333, 'password': 'i'}], {
                user: this.user.id,
                shards: 0,
                Player: require('../music/NamelessPlayer')
            }); 
            //this.commandManager.registerCommand(new TestCommand(this))
            
           // var e = await client.guilds.get("517833347754360832").fetchMembers();
            //console.log(e);
            this.ws.on("INTERACTION_CREATE", async i => {
                var e = new Interaction(this, i)
                await e.ready();
                this.commandManager.handleInteraction(e)
            })
            var files = fs.readdirSync("/home/kyle/bot-evolution/commands")
            var base = "/home/kyle/bot-evolution/commands/"
            for (var f of files) {
                var a = requireUncached(base + f)
                this.commandManager.registerCommand(new a(this));
            }
        })
       
    }
}

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}