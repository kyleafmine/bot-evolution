const Discord = require('discord.js')
const CommandOption = require('./CommandOption')
class SlashCommand {
    /// 
    /// Used for representing a discord-side slash command being pulled to local
    ///
    /**
     * 
     * @param {Discord.Client} client 
     * @param {*} json 
     */
    constructor(client, json) {
        /**
         * @type {Number}
         */
        this.id = json.id
        /**
         * @type {Discord.Client}
         */
        this.client = client
        /**
         * @type {Number}
         */
        this.application_id = json.application_id
        /**
         * @type {Array<CommandOption>}
         */
        this.options = json.options.map(CommandOption)
        /**
         * @type {String}
         */
        this.name = json.name
        /**
         * @type {String}
         */
        this.description = json.description
        /**
         * @type {?Function}
         */
        this.executor = null;

    }
}

module.exports = SlashCommand