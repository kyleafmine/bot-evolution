const Interaction = require('./Interaction')

var Discord = require('discord.js')
module.exports =  class Command {
    constructor(client) {
        /**
         * @type {Discord.Client}
         */
        this.client = client
        this.name = ""
        this.description = ""
        this.guilds = []
        this.options = []


    }

    /**
     * Executes this command, ran on interaction create
     * @param {Interaction} interaction 
     */
    async execute(interaction) {
        await interaction.ack();
    }


}