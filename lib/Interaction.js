const Discord = require('discord.js')

module.exports =  class Interaction {
    static RESPONSETYPE = {
        ACK: 2,
        ACK_SRC: 5,
        RESPOND: 3,
        RESPOND_SRC: 4,
        PONG: 1
    }
    constructor(client, json) {
        /**
         * Token for responding to the command/interaction
         * @type {String}
         */
        this.token = json.token
        this.id = json.id
        /**
         * @type {Discord.Guild}
         */
        this.guild = json.guild_id
        /**
         * @type {Discord.GuildMember}
         */
        this.member = json.member.user.id
        /**
         * @type {String}
         */
        this.name = json.data.name
        /**
         * @type {Number}
         */
        this.commandId = json.data.id
        /**
         * @type {Map<String, ?>}
         */
        this.args = new Map()
        /**
         * @type {Discord.Client}
         */
        this.client = client

        if (json.data.options) {
            for (var i of json.data.options) {
                this.args.set(i.name, i.value)
            }
        }
        /**
         * @type{Discord.TextChannel}
         */
        this.channel = json.channel_id
    }
    async ready() {
        this.guild = await this.client.guilds.fetch(this.guild)
        this.member = await this.guild.members.fetch(this.member)
        this.channel = this.guild.channels.resolve(this.channel)

        // This ish ere for backwards compatibility
        this.author = this.member.user
    }
    /**
     * 
     * @param {String, Discord.APIMessage} message 
     * @param {Number} kind 
     * @returns {null}
     */
    async respond(kind, message, options) {
        if ([2, 1, 5].includes(kind)) {
            // no response msg
            this.client.api.interactions(this.id, this.token).callback.post({data: {
                type: kind
                }
              }).catch(console.log)
        } else {
            // have to get message data
            let m;
            if (message instanceof Discord.APIMessage) {
                m = message.resolveData();
            } else {
                m = Discord.APIMessage.create(this.channel, message, options).resolveData()
            }
            const { data, files } = await m.resolveFiles();
            this.client.api.interactions(this.id, this.token).callback.post({data: {
                type: kind,
                data
                }
              }).then(console.log).catch(console.log)
        }
    }
    /**
     * Shortcut for ACKing a command, deleting the command input
     */
    async ack() {
        return this.respond(2)
    }
}