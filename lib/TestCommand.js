const Command = require('./Command')
var Interaction = require('./Interaction')
var CommandOptionBuilder = require('./CommandOptionBuilder')
module.exports =  class TestCommand extends Command {
    constructor(client) {
        super(client)
        this.name = "test"
        this.description = "it's not funny"
        this.guilds = ["517833347754360832"]
        this.options = [
            {
                name: "name",
                description: "*groans*",
                type: 3,
                required: true
            }
        ]
    }
    /**
     * 
     * @param {Interaction} interaction 
     */
    async execute(interaction) {
        interaction.respond(4, `Hi ${interaction.args.get("name")}, I'm dad!`)
        console.log(interaction.member.user.tag + " just ran the test command!")
    }
}