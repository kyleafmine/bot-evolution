
module.exports =  class CommandOption {
    constructor(json) {
        /**
         * @type {Number}
         */
        this.type = json.type
        /**
         * @type {String}
         */
        this.name =  json.name
        /**
         * @type {String}
         */
        this.description = json.description
        /**
         * @type {Array<Object>}
         */
        this.choices = json.choices
    }
}