module.exports = class CommandOptionBuilder {
    constructor(name) {
        this.name = name
        this.description = ""
        this.type = 3,
        this.required = true

    }
    description(d) {
        this.description = d;
        return this
    }
    type(t) {
        this.type = t;
        return this;

    }
    optional() {
        this.required = false;
        return this
    }
    done() {
        return Object.assign({}, this)
    }
}