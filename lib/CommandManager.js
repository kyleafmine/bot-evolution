const Command = require("./Command")
const fs = require('fs')
const Discord = require('discord.js')
const SlashCommand = require('./SlashCommand')
module.exports = class CommandManager {
    constructor(client) {
        /**
         * @type {Discord.Client}
         */
        this.client = client
        /**
         * @type {Map<String, Command>}
         */
        this.commands = new Map();
        this.commandCache = []
    }
    async checkCommands() {
        this.commandCache = []
        for (var guild of Array.from(this.client.guilds.cache.values())) {
            var commands = await this.client.api.applications(this.client.user.id).guilds(guild.id.toString()).commands.get().catch(console.log)
            if (!commands) {
                continue;
            }
            for (var e of commands) {
                this.commandCache.push(e.name + guild.id)
            }
        }
        var co= await this.client.api.applications(this.client.user.id).commands.get().catch(console.log)
        for (var i of co) {
            this.commandCache.push(i.name)
        }
    }
    /**
     * 
     * @param {Command} command 
     */
    async registerCommand(command, force) {
        this.commands.set(command.name, command)
        if (this.commandCache.includes(command.name) && !force) {
            console.log(`Assuming discord has cached command ${command.name}, skipping`)
            return;
        }
        if (command.guilds.length != 0) {
            return await this.registerGuildCommand(command)
        }
        var g = Object.assign({}, command);
        delete g.guilds
        this.client.api.applications(this.client.user.id).commands.post({
            data: g
        }).then(`Registered command ${command.name}`);
    }
    /**
     * @param {Command} command
     */
    async registerGuildCommand(command) {
        var g = Object.assign({}, command);
        delete g.guilds
        for (var guild of command.guilds) {
            this.client.api.applications(this.client.user.id).guilds(guild.toString()).commands.post({ data: g })
        }
    }

    /**
     * @param {Interaction} i
     */
    async handleInteraction(i) {
        var command = this.commands.get(i.name);
        if (command == null) {
            i.respond(4, "This is not a vaild command! This is a major issue, please contact Kyle")
        } else {
            await command.execute(i)
        }
    }
    async prune() {
        console.log(`Beginning Purge\nConsidering commands from GLOBAL`)
        var commands = await this.client.api.applications(this.client.user.id).commands.get().catch(console.log)
        for (var e of commands) {
            if (!this.commands.has(e.name) || this.commands.get(e.name).guilds.length != 0) {
                this.client.api.applications(this.client.user.id).commands(e.id).delete().then(c => {
                    console.log(`Successfully purged command ${e.name} (global)`)
                }).catch(console.log)
            }
        }
        await this.prune_guilds()
    }
    async prune_guilds() {
        for (var guild of Array.from(this.client.guilds.cache.values())) {
            console.log(`Considering commands from ${guild.id}`)
            var commands = await this.client.api.applications(this.client.user.id).guilds(guild.id.toString()).commands.get().catch(function(){})
            if (!commands) continue;
            for (var e of commands) {
                if (!this.commands.has(e.name) || !this.commands.get(e.name).guilds.includes(guild.id)) {
                    this.client.api.applications(this.client.user.id).guilds(guild.id.toString()).commands(e.id).delete().then(c => {
                        console.log(`Successfully purged command ${e.name} (guild-only)`)
                    }).catch(console.log)
                }
            }
        }
        console.log('The purge has finished!')
    }

    async reload_local() {
        var files = fs.readdirSync("/home/kyle/bot-evolution/commands")
        var base = "/home/kyle/bot-evolution/commands/"
        for (var f of files) {
            var a = requireUncached(base + f)
            this.registerCommand(new a(this.client), true);
        }
        return files.length
    }
}

function requireUncached(module) {
    delete require.cache[require.resolve(module)];
    return require(module);
}