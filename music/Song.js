class Song {
    constructor(json, tag, s, source) {
        this.title = json.info.title
        this.author = json.info.author
        this.track = json.track
        this.tag = tag
        this.info = json.info
        this.query = s
        this.source = source
    }
}

module.exports = Song