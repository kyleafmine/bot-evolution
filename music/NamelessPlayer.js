const { Player } = require('discord.js-lavalink')
const Song = require('./Song')
const MessageAttachment = require('discord.js').MessageAttachment
const request = require('request-promise-native')
class NamelessPlayer extends Player {
    constructor(...options) {
        super(...options)
        this._channel = false
        this.queue = []
        this.oldqueue = []
        this.looping = false
        this.current = false
        this._volume = 100
        this._looped = false
        this.volume(100)
        this._bassboost = false
        //this.on('error', this.manager.client.console.logError)
        this.on('end', (message) => {
            if (message.reason == 'REPLACED') return null;
            if (message.reason !== 'STOPPED') {
                var nextsong = this.queue[1]
                if (!nextsong) {
                    if (this.looping) {
                        this._looped = true
                        this.queue = []
                        for (var s of this.oldqueue) {
                            this.queue.push(s)
                        }
                        
                        this.queue.push(this.current)
                        this.oldqueue = []
                        this.next(true)
                    } else {
                        this._channel.send('Queue finished').then(m => m.delete(3000))
                        //this._channel.setTopic("")
                        this.manager.leave(this.id)
                    }
                } else {
 
                    this.next()
                }

            }
        })
    }
    async init() {
        if (this.img) return true;
        this.img = await require('/home/kyle/queue-render/index')()
        return true;
    }

    bassboost(value = false) {
        this._bassboost = value
        if (value) {
            this.volume(2000)
            this.setEQ(Array(6).fill(0).map((n, i) => ({ band: i, gain: 300})))
            return true
        } else {
            this.volume(25)
            this.setEQ(Array(6).fill(0).map((n, i) => ({ band: i, gain: 0})))
            return true
        }
    }
    /**
     * Queue track(s)
     * @param {Array<Song>|Song} tracks
     */
    async queueSongs(tracks, next) {
        if (tracks instanceof Array) {
            for (var i in tracks) {
                var t = tracks[i]
                t.img = await this.img.fullComposite(t)
                tracks[i] = t
            }
            tracks.forEach(i => this.queue.push(i))
            return true;
        } else {
            tracks.img = await this.img.fullComposite(tracks)
            if (next) {
                this.queue[0] = tracks
                this.queue.unshift(this.current)
            } else {
                this.queue.push(tracks)
            }
            return true;
        }
    }
    /**
     * 
     * @param {Discord.TextChannel} channel 
     */
    registerTextChannel(channel) {
        this._channel = channel
        //this.channeldesc = channel.
        return this
    }


    next(shift, num) {
        if (!shift) {
            if (num) {
                for (var i = 0; i < num; i++) {
                    var removed = this.queue.shift(1)
                    this.oldqueue.push(removed)
                }
            } else {
                var removed = this.queue.shift(1)
            this.oldqueue.push(removed)
            }
        }    
        this.current = this.queue[0]
        this.play(this.current.track)
        if (this._looped) {
            var copy = require('lodash').cloneDeep(this.current)
            delete copy.img
            /*request.post("http://localhost:9004", {
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    cached: true,
                    song: copy,
                    time_to_execute: 4               
                })
            }).then(() => {})*/
        }
        //this._channel.send( `Now playing **${this.current.title}** [${this.current.tag}]`).then(m => m.delete(3000))
        this._channel.send(new MessageAttachment(this.current.img, 'render.png')).then(delete5)
        //this._channel.setTopic(`Now playing: **${this.current.title}** [${this.current.tag}]`)
    }
    setLoop(loop) {
        this.looping = loop
        
    }
    /*play (song, options = {}) {

        super.play(song.track, options)
        this.current = song
        this.volume(this._volume)
        return true
      }
      */
}

module.exports = NamelessPlayer
function delete5(m) {
    setTimeout(() => m.delete(), 5000)
}